<?php

function inline_video_video_plugin() {
  $plugins = array();
  $plugins['video'] = array(
    'title' => t('Inline Video'),
    'vendor url' => 'http://drupal.org/project/inline_video',
    'icon title' => t('Insert video'),
    'settings' => array(),
  );
  
  return $plugins;
}