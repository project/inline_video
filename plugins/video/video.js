Drupal.wysiwyg.plugins['video'] = {
  /**
   * Return whether the passed node belongs to this plugin.
   */
  isNode: function(node) {
    $node = this.getRepresentitiveNode(node);
    return $node.is('img.wysiwyg-video');
  },
  
  /**
   * We need this due all the special cases in the editors
   */
  getRepresentitiveNode: function(node) {
    if (node != null && typeof(node) != 'undefined' && '$' in node) {
      // This case is for the CKeditor, where $(node) != $(node.$)
      return $(node.$);
    }
    // This would be for the TinyMCE and hopefully others
    return $(node);
  },
  
  /**
   * Execute the button.
   */
  invoke: function(data, settings, instanceId) {
    if(data == null && typeof(data) == 'undefined') {
      return;
    }
    
    if (data.format == 'html') {
      // Default
      var options = {
        id: instanceId,
        action: 'insert'
      };
    }
    else {
      // @todo Plain text support.
    }
    if (options.action == 'insert') {
      Drupal.wysiwyg.plugins.video.add_form(data, settings, instanceId);
    }
  },
  
  add_form: function(data, settings, instanceId) {
    var plugin = this;
    var dialogIframe = Drupal.jqui_dialog.iframeSelector();
    
    var btns = {};
    btns[Drupal.t('Insert')] = function () {
      // Test if a url has been typed in
      var size = $(dialogIframe).contents().find('input#edit-video-url').val().length;
      if (size == 0) {
        alert(Drupal.t("Please input a url to a YouTube or Vimeo video first"));
        return;
      }
      var form = $(dialogIframe).contents().find('form#inline-video-form');
      form.ajaxSubmit({
        dataType : 'json',
        method: 'post',
        async: false,
        data: {
          submitvideodetails : 'JSinsert',
        	parent_build_id: Drupal.settings.inline_video.current_form 
        },
        success : function(data, status, xhr, jq) {
          var img = plugin._getPlaceholder(settings, data.type + ':' + data.id);
          Drupal.wysiwyg.instances[instanceId].insert(img);
          $(plugin).dialog("close");
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(Drupal.t("Invalid YouTube or Vimeo url"));
        }
      });
    };
    
    btns[Drupal.t('Cancel')] = function () {
      $(this).dialog("close");
    };
    
    var aurl = Drupal.settings.basePath + 'index.php?q=inline_video/form';
    Drupal.jqui_dialog.open({
      url: aurl,
      buttons: btns,
      width: 400,
      namespace: 'wysiwyg_video'
    });
  },
  
  attach: function(content, settings, instanceId) {
    plugin = this;
    content = content.replace(
      /\[inline_video:(\w+:[a-zA-Z0-9-_]+)\]/g,
      function(orig, id) {
        return plugin._getPlaceholder(settings, id);
      });
    return content;
  },
  
  detach: function(content, settings, instanceId) {
    var $content = $('<div>' + content + '</div>'); // No .outerHTML() in jQuery :(
    $.each(
      $('img.wysiwyg-video', $content),
      function(i, img) {
        $img = $(img);
        $img.replaceWith('[inline_video:'+$img.attr('alt')+']');
      });
    return $content.html();
  },
  
  /**
   * Helper function to return a HTML placeholder.
   */
  _getPlaceholder: function(settings, id) {
    return '<img src="' + settings.path + '/images/spacer.gif" alt="' + id + '" title="Inline Video placeholder" class="wysiwyg-video drupal-content" />';
  },
};
